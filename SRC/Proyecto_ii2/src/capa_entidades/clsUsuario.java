
package capa_entidades;

import java.io.FileInputStream;
import java.sql.Blob;

public class clsUsuario {
    private String usuario;
    private String clave;
    private String correo;
    private String tipo;
    private Blob imagen;
    private FileInputStream flujo;
    private long longitud;
    private boolean estado;

    public clsUsuario(String usuario, String clave, String correo, String tipo) {
        this.usuario = usuario;
        this.clave = clave;
        this.correo = correo;
        this.tipo = tipo;
        this.longitud=0;
        this.estado = true;
    }
    
    
    
    
    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "clsUsuario{" + "usuario=" + usuario + ", clave=" + clave + ", correo=" + correo + ", tipo=" + tipo + '}';
    }

    public Blob getImagen() {
        return imagen;
    }

    public void setImagen(Blob imagen) {
        this.imagen = imagen;
    }

    public FileInputStream getFlujo() {
        return flujo;
    }

    public void setFlujo(FileInputStream flujo) {
        this.flujo = flujo;
    }

    public long getLongitud() {
        return longitud;
    }

    public void setLongitud(long longitud) {
        this.longitud = longitud;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    
    
    
    
    
}
