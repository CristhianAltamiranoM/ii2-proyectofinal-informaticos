
package capa_entidades;

public class clsBackUp{
    private clsEvento evento;
    private String ruta;

    public clsBackUp(clsEvento evento, String ruta) {
        this.evento = evento;
        this.ruta = ruta;
    }

    public clsEvento getEvento() {
        return evento;
    }

    public void setEvento(clsEvento evento) {
        this.evento = evento;
    }

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    
    
}