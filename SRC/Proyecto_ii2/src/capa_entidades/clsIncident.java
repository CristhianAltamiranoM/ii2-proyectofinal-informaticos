
package capa_entidades;

public class clsIncident{
    private clsEvento evento;
    private String incidente;
    private String descripcion;
    private String motivo;
    private String evidencias;
    private String solucion;

    public clsIncident(clsEvento evento, String incidente, String descripcion, String motivo, String evidencias, String solucion) {
        this.evento = evento;
        this.incidente = incidente;
        this.descripcion = descripcion;
        this.motivo = motivo;
        this.evidencias = evidencias;
        this.solucion = solucion;
    }

   

    

    
    public String getIncidente() {
        return incidente;
    }

    public void setIncidente(String incidente) {
        this.incidente = incidente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getEvidencias() {
        return evidencias;
    }

    public void setEvidencias(String evidencias) {
        this.evidencias = evidencias;
    }

    public String getSolucion() {
        return solucion;
    }

    public void setSolucion(String solucion) {
        this.solucion = solucion;
    }

    public clsEvento getEvento() {
        return evento;
    }

    public void setEvento(clsEvento evento) {
        this.evento = evento;
    }
    
    
}
