package capa_entidades;

public class clsHealth{
    private clsEvento evento;
    private String problema;  

    public clsHealth(clsEvento evento, String problema) {
        this.evento = evento;
        this.problema = problema;
    }

    

    public String getProblema() {
        return problema;
    }

    public void setProblema(String problema) {
        this.problema = problema;
    }

    public clsEvento getEvento() {
        return evento;
    }

    public void setEvento(clsEvento evento) {
        this.evento = evento;
    }
    
    
    
}
