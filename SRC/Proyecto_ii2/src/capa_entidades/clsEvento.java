package capa_entidades;

public class clsEvento {
    private int idEvento;
    private clsBaseDatos Base;
    private String fecha;
    private String hora;
    private String tipo;
    private boolean estado;

    public clsEvento(clsBaseDatos codigoBase, String fecha, String hora, boolean estado) {
        this.idEvento = 0;
        this.Base = codigoBase;
        this.fecha = fecha;
        this.hora = hora;
        this.estado = estado;
    }

    public int getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(int idEvento) {
        this.idEvento = idEvento;
    }

    public clsBaseDatos getCodigoBase() {
        return Base;
    }

    public void setCodigoBase(clsBaseDatos codigoBase) {
        this.Base = codigoBase;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getTipo(){
        return tipo;
    }
    
    public void setTipo(String tipo){
        this.tipo=tipo;
    }

    
}
