
package capa_entidades;

import java.io.FileInputStream;
import java.sql.Blob;

public class clsBaseDatos {
    private String codigo;
    private Blob script;
    private String nombre;
    private String fechaCarga;
    private String tipo;
    private FileInputStream flujo;
    private long longitud;

    public clsBaseDatos(String codigo, Blob script, String nombre, String fechaCarga, String tipo) {
        this.codigo = codigo;
        this.script = script;
        this.nombre = nombre;
        this.fechaCarga = fechaCarga;
        this.tipo = tipo;
    }

    public clsBaseDatos(String codigo, String nombre, String fechaCarga, String tipo, FileInputStream flujo, long longitud) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.fechaCarga = fechaCarga;
        this.tipo = tipo;
        this.flujo = flujo;
        this.longitud = longitud;
    }
    
    

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaCarga() {
        return fechaCarga;
    }

    public void setFechaCarga(String fechaCarga) {
        this.fechaCarga = fechaCarga;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Blob getScript() {
        return script;
    }

    public void setScript(Blob script) {
        this.script = script;
    }

    public FileInputStream getFlujo() {
        return flujo;
    }

    public void setFlujo(FileInputStream flujo) {
        this.flujo = flujo;
    }

    public long getLongitud() {
        return longitud;
    }

    public void setLongitud(long longitud) {
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "nombre='" + nombre + "'";
    }

    

    
    
}
