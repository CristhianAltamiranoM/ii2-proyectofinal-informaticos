
package capa_entidades;

import java.sql.Blob;

public class clsTesting extends clsEvento {
    private clsEvento evento;
    private String autor;
    private String tipoT;
    private String solicitante;
    private Blob hojaS;
    private String program_prueba;

    public clsTesting(clsEvento evento, String autor, String tipoT, String solicitante, Blob hojaS, String program_prueba, clsBaseDatos codigoBase, String fecha, String hora, boolean estado) {
        super(codigoBase, fecha, hora, estado);
        this.evento = evento;
        this.autor = autor;
        this.tipoT = tipoT;
        this.solicitante = solicitante;
        this.hojaS = hojaS;
        this.program_prueba = program_prueba;
    }

   
    

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTipoT() {
        return tipoT;
    }

    public void setTipoT(String tipoT) {
        this.tipoT = tipoT;
    }

    public String getSolicitante() {
        return solicitante;
    }

    public void setSolicitante(String solicitante) {
        this.solicitante = solicitante;
    }

    public Blob getHojaS() {
        return hojaS;
    }

    public void setHojaS(Blob hojaS) {
        this.hojaS = hojaS;
    }

    public String getProgram_prueba() {
        return program_prueba;
    }

    public void setProgram_prueba(String program_prueba) {
        this.program_prueba = program_prueba;
    }

    


   
    
    
}
