
package capa_entidades;

import java.io.FileInputStream;
import java.sql.Blob;

public class clsReporting{
    private clsEvento evento;
    private String Solicitante;
    private Blob hoja_solicitud;
    private String requerimiento;
    private String encargado;
    private Blob reporte_entregado;
    private String motivo;
    private FileInputStream flujo1;
    private FileInputStream flujo2;
    private long long1;
    private long long2;

    public clsReporting(clsEvento evento, String Solicitante,String requerimiento, String encargado,String motivo) {
        this.evento = evento;
        this.Solicitante = Solicitante;
        this.requerimiento = requerimiento;
        this.encargado = encargado;
        this.motivo = motivo;
    }

    

    

    

    

    public String getSolicitante() {
        return Solicitante;
    }

    public void setSolicitante(String Solicitante) {
        this.Solicitante = Solicitante;
    }

    public Blob getHoja_solicitud() {
        return hoja_solicitud;
    }

    public void setHoja_solicitud(Blob hoja_solicitud) {
        this.hoja_solicitud = hoja_solicitud;
    }

    public String getRequerimiento() {
        return requerimiento;
    }

    public void setRequerimiento(String requerimiento) {
        this.requerimiento = requerimiento;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public Blob getReporte_entregado() {
        return reporte_entregado;
    }

    public void setReporte_entregado(Blob reporte_entregado) {
        this.reporte_entregado = reporte_entregado;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public clsEvento getEvento() {
        return evento;
    }

    public void setEvento(clsEvento evento) {
        this.evento = evento;
    }

    public FileInputStream getFlujo1() {
        return flujo1;
    }

    public void setFlujo1(FileInputStream flujo1) {
        this.flujo1 = flujo1;
    }

    public FileInputStream getFlujo2() {
        return flujo2;
    }

    public void setFlujo2(FileInputStream flujo2) {
        this.flujo2 = flujo2;
    }

    public long getLong1() {
        return long1;
    }

    public void setLong1(long long1) {
        this.long1 = long1;
    }

    public long getLong2() {
        return long2;
    }

    public void setLong2(long long2) {
        this.long2 = long2;
    }
    
    
}
