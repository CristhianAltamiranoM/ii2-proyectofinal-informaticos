
package capa_presentacion;

import capa_accesoDatos.clsBackUpDAO;
import capa_accesoDatos.clsBaseDatosDAO;
import capa_accesoDatos.clsEventoDAO;
import capa_entidades.clsBaseDatos;
import capa_entidades.clsEvento;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class frmMantenimientoEventos extends javax.swing.JDialog {
    private clsEvento evento;
    private clsEvento seleccionado;
    public frmMantenimientoEventos(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.evento=null;
        this.seleccionado=null;
        CargarBasesDatos();
        CargarDatos();
    }
    
    private void controles(boolean estado){
        this.txtFecha.setEnabled(estado);
        this.txtHora.setEnabled(estado);
        this.cmbBasesDatos.setEnabled(estado);
    }
    
    private void limpiar(){
        txtFecha.setText("");
        txtHora.setText("");
        cmbBasesDatos.setSelectedIndex(0);
        controles(true);
        this.evento=null;
        this.seleccionado=null;
    }
    
    private void CargarBasesDatos(){
        try {
            clsBaseDatosDAO dao = new clsBaseDatosDAO();
            DefaultComboBoxModel modelo = new DefaultComboBoxModel();
            ArrayList<clsBaseDatos> lista = dao.listar_bd();
            
            for (clsBaseDatos baseDatos : lista) {
                modelo.addElement(baseDatos);
            }
            
            this.cmbBasesDatos.setModel(modelo);
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
        }
    }
    
    private void CargarDatos(){
        try {
            clsEventoDAO dao = new clsEventoDAO();
            String columnas[]={"ID Evento","Fecha","Hora","Base de Datos","Tipo","Estado"};
            DefaultTableModel modelo = new DefaultTableModel(columnas, 0);
            Object matriz[][] = new Object[1][6];
            
            ArrayList<clsEvento> lista = dao.listar_eventos();
            for (int i = 0; i < lista.size(); i++) {
                clsEvento obj = lista.get(i);
                matriz[0][0] = obj.getIdEvento();
                matriz[0][1] = obj.getFecha();
                matriz[0][2] = obj.getHora();
                matriz[0][3] = obj.getCodigoBase().getCodigo();
                matriz[0][4] = obj.getTipo();
                matriz[0][5] = obj.isEstado();
                modelo.addRow(matriz[0]);
                
            }
            
            this.tblDatos.setModel(modelo);
                    
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cmbBasesDatos = new javax.swing.JComboBox<>();
        txtFecha = new javax.swing.JFormattedTextField();
        jLabel3 = new javax.swing.JLabel();
        cmbTipoEvento = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        txtHora = new javax.swing.JFormattedTextField();
        jButton1 = new javax.swing.JButton();
        txtId = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblDatos = new javax.swing.JTable();
        btnEliminar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(java.awt.SystemColor.controlShadow);
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "NUEVO EVENTO", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.ABOVE_TOP));

        jLabel1.setText("Fecha:");

        jLabel2.setText("Base de Datos:");

        cmbBasesDatos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        txtFecha.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter()));

        jLabel3.setText("Tipo de Evento:");

        cmbTipoEvento.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "--Seleccione--", "BackUp", "Health", "Incident", "Reporting", "Testing" }));
        cmbTipoEvento.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbTipoEventoItemStateChanged(evt);
            }
        });

        jLabel4.setText("Hora:");

        txtHora.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.DateFormatter(java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT))));

        jButton1.setText("INGRESAR DATOS");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cmbBasesDatos, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtHora)
                                .addGap(68, 68, 68))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cmbTipoEvento, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(51, 51, 51)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 51, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(txtId, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtHora, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cmbBasesDatos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cmbTipoEvento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap(41, Short.MAX_VALUE))
        );

        tblDatos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tblDatos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblDatosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblDatos);

        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/minus.png"))); // NOI18N
        btnEliminar.setText("ELIMINAR");
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/icons8_Multiply_32px.png"))); // NOI18N
        btnCerrar.setText("CERRAR");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        btnGuardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/guardar.png"))); // NOI18N
        btnGuardar.setText("GUARDAR");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(197, 197, 197)
                        .addComponent(btnCerrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 452, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                    .addComponent(btnCerrar, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)
                    .addComponent(btnGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, 66, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
       JOptionPane.showMessageDialog(rootPane, "Registro Exitoso");
       CargarDatos();
       limpiar();
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void cmbTipoEventoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbTipoEventoItemStateChanged

    }//GEN-LAST:event_cmbTipoEventoItemStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
       controles(false);
        switch(cmbTipoEvento.getSelectedIndex()){
            case 1:
                 
                try {
                    clsEventoDAO dao = new clsEventoDAO();
                    clsBaseDatos base = (clsBaseDatos) cmbBasesDatos.getSelectedItem();
                    evento = new clsEvento(base, txtFecha.getText(), txtHora.getText(), true);
                    evento.setTipo(cmbTipoEvento.getSelectedItem().toString());
                    dao.insertar_evento(evento);
                    
                    frmNuevoBackUp frm= new frmNuevoBackUp(null, true,evento);
                    frm.setTitle("REGISTRO DE BACKUP");
                    frm.setVisible(true);
                    
                    break;
                    
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
                }
            
            case 2:
                  
                try {
                    clsEventoDAO dao = new clsEventoDAO();
                    clsBaseDatos base = (clsBaseDatos) cmbBasesDatos.getSelectedItem();
                    evento = new clsEvento(base, txtFecha.getText(), txtHora.getText(), true);
                    evento.setTipo(cmbTipoEvento.getSelectedItem().toString());
                    dao.insertar_evento(evento);
                    
                    frmNuevoHealth frm= new frmNuevoHealth(null, true,evento);
                    frm.setTitle("REGISTRO DE HEALT");
                    frm.setVisible(true);
                    
                    break;
                    
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
                }
            case 3:
                 
                try {
                    clsEventoDAO dao = new clsEventoDAO();
                    clsBaseDatos base = (clsBaseDatos) cmbBasesDatos.getSelectedItem();
                    evento = new clsEvento(base, txtFecha.getText(), txtHora.getText(), true);
                    evento.setTipo(cmbTipoEvento.getSelectedItem().toString());
                    dao.insertar_evento(evento);
                    
                    frmNuevoIncident frm= new frmNuevoIncident(null, true,evento);
                    frm.setTitle("REGISTRO DE INCIDENT");
                    frm.setVisible(true);
                    
                    break;
                    
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
                }
                
            case 4:
                 
                try {
                    clsEventoDAO dao = new clsEventoDAO();
                    clsBaseDatos base = (clsBaseDatos) cmbBasesDatos.getSelectedItem();
                    evento = new clsEvento(base, txtFecha.getText(), txtHora.getText(), true);
                    evento.setTipo(cmbTipoEvento.getSelectedItem().toString());
                    dao.insertar_evento(evento);
                    
                    frmNuevoReporting frm= new frmNuevoReporting(null, true,evento);
                    frm.setTitle("REGISTRO DE REPORTING");
                    frm.setVisible(true);
                    
                    break;
                    
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
                }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        try {
            switch(seleccionado.getTipo()){
                case "BackUp":
                    clsBackUpDAO bkdao = new clsBackUpDAO();
                    clsEventoDAO evento = new clsEventoDAO();
                    bkdao.eliminar_backup(seleccionado.getIdEvento());
                    JOptionPane.showMessageDialog(rootPane, "Eliminacion Exitosa");
                    
                    
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void tblDatosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblDatosMouseClicked
        int pos = tblDatos.getSelectedRow();
        DefaultTableModel modelo = (DefaultTableModel) this.tblDatos.getModel();
        if(pos!=-1){
            try {
                clsEventoDAO dao = new clsEventoDAO();
                clsEvento e = dao.obtener_evento(Integer.parseInt(modelo.getValueAt(pos, 0).toString()));
                
            } catch (Exception e) {
                JOptionPane.showMessageDialog(rootPane, "Error: "+e.getMessage());
            }
        }
    }//GEN-LAST:event_tblDatosMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmMantenimientoEventos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmMantenimientoEventos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmMantenimientoEventos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmMantenimientoEventos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frmMantenimientoEventos dialog = new frmMantenimientoEventos(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JComboBox<String> cmbBasesDatos;
    private javax.swing.JComboBox<String> cmbTipoEvento;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblDatos;
    private javax.swing.JFormattedTextField txtFecha;
    private javax.swing.JFormattedTextField txtHora;
    private javax.swing.JLabel txtId;
    // End of variables declaration//GEN-END:variables
}
