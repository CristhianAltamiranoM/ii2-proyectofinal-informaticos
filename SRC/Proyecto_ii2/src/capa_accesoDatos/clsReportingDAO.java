
package capa_accesoDatos;

import capa_entidades.clsEvento;
import capa_entidades.clsReporting;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class clsReportingDAO {
    public void insertar_reporting(clsReporting obj)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="insert into reporting(idEvento,solicitante,hojaS,requerimiento,encargado,reporte,motivo) values(?,?,?,?,?,?,?)";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1,obj.getEvento().getIdEvento());
        pst.setString(2,obj.getSolicitante());
        pst.setBlob(3,obj.getFlujo1(),obj.getLong1());
        pst.setString(4,obj.getRequerimiento());
        pst.setString(5,obj.getEncargado());
        pst.setBlob(6,obj.getFlujo2(),obj.getLong2());
        pst.setString(7,obj.getMotivo());
        pst.executeUpdate();
    }
    
    public void eliminar_reporting(int id)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="delete from reporting where idEvento=?";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1, id);
        pst.executeUpdate();
    }
    public ArrayList<clsReporting> listar_reportings() throws Exception{
        ArrayList<clsReporting> Lista=new ArrayList<>();
        Connection con=clsConexion.getConexion();
        String sql="Select * from reporting";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento evento= baseD.obtener_evento(rs.getInt("idEvento"));
            clsReporting obj = new clsReporting(evento, rs.getString("solicitante"),
                                                rs.getString("requerimiento"),rs.getString("encargado"), 
                                                rs.getString("motivo"));
            Lista.add(obj);
        }
        return Lista;
    }
    
    public clsReporting obtener_reporting(int evento) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from incident where idEvento="+evento+"";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento evet= baseD.obtener_evento(rs.getInt("idEvento"));
            clsReporting obj = new clsReporting(evet, rs.getString("solicitante"), 
                                                rs.getString("requerimiento"),rs.getString("encargado"), 
                                                rs.getString("motivo"));
           
            return obj;
        }
        return null;
    
    }
}
