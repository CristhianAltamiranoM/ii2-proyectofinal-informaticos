
package capa_accesoDatos;

import capa_entidades.clsEvento;
import capa_entidades.clsHealth;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public class clsHealtDAO {
    public void insertar_health(clsHealth obj)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="insert into health(idEvento,problema) values(?,?)";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1,obj.getEvento().getIdEvento());
        pst.setString(2,obj.getProblema());
        pst.executeUpdate();
    }
    
    public void eliminar_health(int id)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="delete from health where idEvento=?";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1, id);
        pst.executeUpdate();
    }
    public ArrayList<clsHealth> listar_healths() throws Exception{
        ArrayList<clsHealth> Lista=new ArrayList<>();
        Connection con=clsConexion.getConexion();
        String sql="Select * from health";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento evento= baseD.obtener_evento(rs.getInt("idEvento"));
            clsHealth obj = new clsHealth(evento, rs.getString("problema"));
            Lista.add(obj);
        }
        return Lista;
    }
    
    public clsHealth obtener_health(int evento) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from health where idEvento="+evento+"";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento ev= baseD.obtener_evento(rs.getInt("idEvento"));
            clsHealth obj = new clsHealth(ev, rs.getString("problema"));
           
            return obj;
        }
        return null;
    
    }
}
