package capa_accesoDatos;

import capa_entidades.clsBaseDatos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public class clsBaseDatosDAO {
    public void insertar_BD(clsBaseDatos obj)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="insert into base_datos(codigo,script,nombre,fecha,tipo)values(?,?,?,?,?)";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setString(1,obj.getCodigo());
        pst.setBlob(2,obj.getFlujo(),obj.getLongitud());
        pst.setString(3,obj.getNombre());
        pst.setString(4,obj.getFechaCarga());
        pst.setString(5,obj.getTipo());
        pst.executeUpdate();
    }

    public void eliminar_bd(String codigo)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="delete from base_datos where codigo=?";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setString(1, codigo);
        pst.executeUpdate();
    }
    public ArrayList<clsBaseDatos> listar_bd() throws Exception{
        ArrayList<clsBaseDatos> Lista=new ArrayList<>();
        Connection con=clsConexion.getConexion();
        String sql="Select * from base_datos";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsBaseDatos obj = new clsBaseDatos(rs.getString("codigo"), rs.getBlob("script"),
                                                rs.getString("nombre"), rs.getString("fecha"), 
                                                rs.getString("tipo"));
           
            Lista.add(obj);
        }
        return Lista;
    }
    
    public clsBaseDatos obtenerBaseDatos(String codigo) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from base_datos where codigo='"+codigo+"'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsBaseDatos obj = new clsBaseDatos(rs.getString("codigo"), rs.getBlob("script"),
                                                rs.getString("nombre"), rs.getString("fecha"), 
                                                rs.getString("tipo"));
           
            return obj;
        }
        return null;
    
    }
}
