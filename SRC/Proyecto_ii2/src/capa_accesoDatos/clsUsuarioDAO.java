
package capa_accesoDatos;

import capa_entidades.clsUsuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class clsUsuarioDAO {
     public void insertar_usuario(clsUsuario obj)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="insert into usuario(user,pass,correo,tipo,imagen)values(?,?,?,?,?)";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setString(1,obj.getUsuario());
        pst.setString(2,obj.getClave());
        pst.setString(3,obj.getCorreo());
        pst.setString(4,obj.getTipo());
        pst.setBlob(5,obj.getFlujo(),obj.getLongitud());
        pst.executeUpdate();
    }

    public void eliminar_usuario(String user)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="delete from usuario where user=?";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setString(1, user);
        pst.executeUpdate();
    }
    public ArrayList<clsUsuario> listar_usuarios() throws Exception{
        ArrayList<clsUsuario> Lista=new ArrayList<>();
        Connection con=clsConexion.getConexion();
        String sql="Select * from usuario";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsUsuario obj = new clsUsuario(rs.getString("user"), rs.getString("pass"), 
                                            rs.getString("correo"), rs.getString("tipo"));
            obj.setImagen(rs.getBlob("imagen"));
           
            Lista.add(obj);
        }
        return Lista;
    }
    
    public clsUsuario obtener_usuario(String user) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from usuario where user='"+user+"'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsUsuario obj = new clsUsuario(rs.getString("user"), rs.getString("pass"), 
                                            rs.getString("correo"), rs.getString("tipo"));
            obj.setImagen(rs.getBlob("imagen"));
           
            return obj;
        }
        return null;
    
    }
    
    public boolean validar_usuario(String user, String pass) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from usuario";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            if(rs.getString("user").equals(user) && rs.getString("pass").equals(pass))
                return true;
           
        }
        return false;
    }
}
