
package capa_accesoDatos;

import capa_entidades.clsBaseDatos;
import capa_entidades.clsEvento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class clsEventoDAO {
    public void insertar_evento(clsEvento obj)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="insert into evento(codigoBase,Fecha,hora,tipo)values(?,?,?,?)";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setString(1,obj.getCodigoBase().getCodigo());
        pst.setString(2,obj.getFecha());
        pst.setString(3,obj.getHora());
        pst.setString(4,obj.getTipo());
        pst.executeUpdate();
    }
    
    public void eliminar_evento(int id)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="delete from evento where idEvento=?";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1, id);
        pst.executeUpdate();
    }
    public ArrayList<clsEvento> listar_eventos() throws Exception{
        ArrayList<clsEvento> Lista=new ArrayList<>();
        Connection con=clsConexion.getConexion();
        String sql="Select * from evento";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsBaseDatosDAO baseD = new clsBaseDatosDAO();
            clsBaseDatos base = baseD.obtenerBaseDatos(rs.getString("codigoBase"));
            clsEvento obj = new clsEvento(base, rs.getString("Fecha"), 
                                          rs.getString("Hora"), rs.getBoolean("estado"));
            obj.setIdEvento(rs.getInt("idEvento"));
            obj.setTipo(rs.getString("tipo"));
           
            Lista.add(obj);
        }
        return Lista;
    }
    
    public clsEvento obtener_evento(int evento) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from evento where idEvento="+evento+"";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsBaseDatosDAO baseD = new clsBaseDatosDAO();
            clsBaseDatos base = baseD.obtenerBaseDatos(rs.getString("codigoBase"));
            clsEvento obj = new clsEvento(base, rs.getString("Fecha"), 
                                          rs.getString("Hora"), rs.getBoolean("estado"));
            obj.setIdEvento(rs.getInt("idEvento"));
            obj.setTipo(rs.getString("tipo"));
           
            return obj;
        }
        return null;
    
    }
    
    public clsEvento obtener_evento_ultimo() throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="select max(idEvento) as idEvento,codigoBase,Fecha,Hora,estado from evento GROUP by idEvento";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsBaseDatosDAO baseD = new clsBaseDatosDAO();
            clsBaseDatos base = baseD.obtenerBaseDatos(rs.getString("codigoBase"));
            clsEvento obj = new clsEvento(base, rs.getString("Fecha"), 
                                          rs.getString("hora"), rs.getBoolean("estado"));
            obj.setIdEvento(rs.getInt("idEvento"));
           
            return obj;
        }
        return null;
    
    }
}
