
package capa_accesoDatos;

import capa_entidades.clsBackUp;
import capa_entidades.clsEvento;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class clsBackUpDAO {
    public void insertar_backup(clsBackUp obj)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="insert into backup(idEvento,ruta) values(?,?)";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1,obj.getEvento().getIdEvento());
        pst.setString(2,obj.getRuta());
        pst.executeUpdate();
    }
    
    public void eliminar_backup(int id)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="delete from backup where idEvento=?";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1, id);
        pst.executeUpdate();
    }
    public ArrayList<clsBackUp> listar_backups() throws Exception{
        ArrayList<clsBackUp> Lista=new ArrayList<>();
        Connection con=clsConexion.getConexion();
        String sql="Select * from backup";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento evento= baseD.obtener_evento(rs.getInt("idEvento"));
            clsBackUp obj = new clsBackUp(evento, rs.getString("ruta"));
            Lista.add(obj);
        }
        return Lista;
    }
    
    public clsBackUp obtener_backup(int evento) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from evento where idEvento="+evento+"";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento ev= baseD.obtener_evento(rs.getInt("idEvento"));
            clsBackUp obj = new clsBackUp(ev, rs.getString("ruta"));
           
            return obj;
        }
        return null;
    
    }
}
