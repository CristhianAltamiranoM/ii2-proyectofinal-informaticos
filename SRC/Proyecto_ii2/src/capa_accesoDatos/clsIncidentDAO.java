package capa_accesoDatos;

import capa_entidades.clsEvento;
import capa_entidades.clsIncident;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class clsIncidentDAO {
    public void insertar_incident(clsIncident obj)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="insert into incident(idEvento,incidente,descripcion,motivo,evidencias,solucion) values(?,?,?,?,?,?)";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1,obj.getEvento().getIdEvento());
        pst.setString(2,obj.getIncidente());
        pst.setString(3,obj.getDescripcion());
        pst.setString(4,obj.getMotivo());
        pst.setString(5,obj.getEvidencias());
        pst.setString(6,obj.getSolucion());
        pst.executeUpdate();
    }
    
    public void eliminar_incident(int id)throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="delete from incident where idEvento=?";
        PreparedStatement pst=con.prepareStatement(sql);
        pst.setInt(1, id);
        pst.executeUpdate();
    }
    public ArrayList<clsIncident> listar_incidents() throws Exception{
        ArrayList<clsIncident> Lista=new ArrayList<>();
        Connection con=clsConexion.getConexion();
        String sql="Select * from incident";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento evento= baseD.obtener_evento(rs.getInt("idEvento"));
            clsIncident obj = new clsIncident(evento, rs.getString("insidente"), 
                                            rs.getString("descripcion"), rs.getString("motivo"), 
                                            rs.getString("evidencias"), rs.getString("solucion"));
            Lista.add(obj);
        }
        return Lista;
    }
    
    public clsIncident obtener_incident(int evento) throws Exception{
        Connection con=clsConexion.getConexion();
        String sql="Select * from incident where idEvento="+evento+"";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next())
        {
            clsEventoDAO baseD = new clsEventoDAO();
            clsEvento evet= baseD.obtener_evento(rs.getInt("idEvento"));
            clsIncident obj = new clsIncident(evet, rs.getString("insidente"), 
                                            rs.getString("descripcion"), rs.getString("motivo"), 
                                            rs.getString("evidencias"), rs.getString("solucion"));
           
            return obj;
        }
        return null;
    
    }
}
